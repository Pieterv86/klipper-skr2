# Klipper-SKR2

Configuration of klipper for SKRv2
Base printer is a Ender3 V2 but has following mods:
- dual Z axis
- dual z stepper driver & motor
- noctua fan's
- Bondtech extruder
- micro-swiss all metal hotend
- capricorn bowden
- CR-Touch with custom wiring for this board
- TMC 2209 Drivers for X,Y and extruder
- TMC 2208 Drivers for both Z motors
